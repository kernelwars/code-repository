monthlyInterestRate=annualInterestRate/12
MonthlyPaymentLowerBound=balance/12
MonthlyPaymentUpperBound=(balance*(1+monthlyInterestRate)**12)/12
ans=(MonthlyPaymentLowerBound+MonthlyPaymentUpperBound)/2
e=0.5
while balance > 0:  
    nbalance = balance
    for month in range (1, 13):       
        nbalance=(nbalance - ans)*(1+monthlyInterestRate)
    if nbalance>e:
        nbalance=balance
        MonthlyPaymentLowerBound=ans
        ans=(MonthlyPaymentLowerBound+MonthlyPaymentUpperBound)/2
    elif abs(nbalance)<e and abs(nbalance)>0:
         print ('Lowest Payment: ' + str(round(ans,2)))
         break
    else: 
        MonthlyPaymentUpperBound=ans
        nbalance=balance
        ans=(MonthlyPaymentLowerBound+MonthlyPaymentUpperBound)/2
