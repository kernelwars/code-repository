import string

def buildCoder(shift):
    """
    Returns a dict that can apply a Caesar cipher to a letter.
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation, numbers, and spaces.

    shift: 0 <= int < 26
    returns: dict
    """
    ### TODO 
    coder = {}
    lshift = zip(string.ascii_lowercase, string.ascii_lowercase[shift:] + string.ascii_lowercase[:shift])
    ushift = zip(string.ascii_uppercase, string.ascii_uppercase[shift:] + string.ascii_uppercase[:shift])
    return dict(lshift + ushift)

import string

def applyCoder(text, coder):
    """
    Applies the coder to the text. Returns the encoded text.

    text: string
    coder: dict with mappings of characters to shifted characters
    returns: text after mapping coder chars to original text
    """
    ### TODO
    
    for e in text:
        if e in coder:
            text.replace(e,coder[e])
        
    return text

import string

def applyShift(text, shift):
    """
    Given a text, returns a new text Caesar shifted by the given shift
    offset. Lower case letters should remain lower case, upper case
    letters should remain upper case, and all other punctuation should
    stay as it is.

    text: string to apply the shift to
    shift: amount to shift the text (0 <= int < 26)
    returns: text after being shifted by specified amount.
    """
    ### TODO.
    ### HINT: This is a wrapper function.
    ans = applyCoder(text, buildCoder(shift))
    return ans

import string

def findBestShift(wordList, text):
    """
    Finds a shift key that can decrypt the encoded text.

    text: string
    returns: 0 <= int < 26
    """
    ### TODO
    number_of_realwords_for_each_iteration_with_key = {}
    realwords = 0
    list = []
    y = text.split(' ')
    for i in range(26):
        for j in y:
            if isWord(wordList, applyShift(j, i)) == True:
                realwords = realwords + 1      
        number_of_realwords_for_each_iteration_with_key[realwords] = i
        realwords = 0
    for k in number_of_realwords_for_each_iteration_with_key:
        list.append(k)
    return number_of_realwords_for_each_iteration_with_key[max(list)]
import string

def decryptStory():
    """
    Using the methods you created in this problem set,
    decrypt the story given by the function getStoryString().
    Once you decrypt the message, be sure to include as a comment
    your decryption of the story.

    returns: string - story in plain text
    """
    ### TODO
    
    encryptedStory = getStoryString()
    shift = findBestShift(loadWords(), encryptedStory)
    return applyShift(encryptedStory, shift)
def reverseString(aStr):
    """
    Given a string, recursively returns a reversed copy of the string.
    For example, if the string is 'abc', the function returns 'cba'.
    The only string operations you are allowed to use are indexing,
    slicing, and concatenation.
    
    aStr: a string
    returns: a reversed string
    """
    ### TODO
    if len(aStr) <= 1:
        return aStr
    else:
        return aStr[-1] + reverseString(aStr[:-1])
def x_ian(x, word):
    """
    Given a string x, returns True if all the letters in x are
    contained in word in the same order as they appear in x.
    
    x: a string
    word: a string
    returns: True if word is x_ian, False otherwise
    """
    ###TODO
    if x == '':
        return True

    elif word == '':
        return False
    if x in word:
        return True

#If both first letters the same drop them both
#and call x_ian again
    if x[0] == word[0]:
        return x_ian(x[1:], word[1:])
        return True
#Else drop the first letter of word
#and call x_ian again
    else:
        return x_ian(x, word[1:])
        return False
def insertNewlines(text, lineLength):
    """
    Given text and a desired line length, wrap the text as a typewriter would.
    Insert a newline character ("\n") after each word that reaches or exceeds
    the desired line length.

    text: a string containing the text to wrap.
    lineLength: the number of characters to include on a line before wrapping
        the next word.
    returns: a string, with newline characters inserted appropriately. 
    """
    ### TODO
     length=len(text)
    if length==0 or length<=lineLength:
        return text
   
    else:
        for i in range(lineLength, length):
            if text[i]!=' ':
                 return (text[0:i]+'\n'+insertNewlines(text[i+1:]), lineLength)
