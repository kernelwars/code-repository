from math import pow
def withinEpsilon(x, y, epsilon):
    return abs(x - y) <= epsilon     
def isEven(i):

    return i%2 == 0
def computeRoot(pwr, val, epsilon):
    '''
    Uses Newton's method to find and return a root of a polynomial function.
    Returns a list containing the root and the number of iterations required
    to get to the root.
 
    poly: list of numbers, length > 1.
         Represents a polynomial function containing at least one real root.
         The derivative of this polynomial function at x_0 is not 0.
    x_0: float
    epsilon: float > 0
    returns: list [float, int]
    '''
    # FILL IN YOUR CODE HERE...
    #assert type(pwr) == int and type(val) == float and type(epsilon) == float
    assert pwr > 0 and epsilon > 0
    #if isEven(pwr) and val < 0: 
        #return None 
    low = -abs(val)
    high = max(abs(val), 1.0)
    ans = (high + low)/2.0
    while not withinEpsilon(ans**pwr, val, epsilon):
    #print 'ans =', ans, 'low =', low, 'high =', high
        if ans**pwr < val:
            low = ans 
        else: 
            high = ans
            ans = (high + low)/2.0
    return ans

