def myLog(x, b):
    '''
    x: a positive integer
    b: a positive integer

    returns: log_b(x), or, the logarithm of x relative to a base b.
    '''
    # Your Code Here
    count = -1
    while x > 0:
        x /= b
        count += 1
        if x == 0:
            return count
