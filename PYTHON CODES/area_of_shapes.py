from math import pi, sqrt
def area(r,shape_constant):
     """return the area of a shape of length r"""
     assert r > 0 #must be positive
     return r * r * shape_constant

def area_square(r):
     """Returns the area of a square with side r"""
     return area(r,1)

def area_circle(r):
     """Returns the area of a circle with radius r"""
     return area(r,pi)

def area_hexagon(r):
     """Returns the area of a regular hexagon with side length r"""
     return area(r, 3 * sqrt(3)/2)
