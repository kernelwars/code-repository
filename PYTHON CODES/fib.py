def fib(n):
    """Computing the nth Fib number, for n>= 2"""
    prev, curr = 0,1
    k = 2 #tracks which fib num is current
    while k < n:
        prev, curr = curr, prev + curr
        k = k + 1
    return curr    

        
