def myLog(x, b):
     count = -1
     while x > 0:
          x /= b
          count += 1
          if x == 0:
               return count
