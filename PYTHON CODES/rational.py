#Use
def mul_rational(x,y):
     return rational(numer(x) * numer(y), denom(x) * denom(y))

def add rational(x,y):
     nx, dx = numer(x), denom(x)
     ny, dy = numer(y), denom(y)

def eq_rational(x,y):
     
     return numer(x) * denom(y) == numer(y) * denom(x)
# Representation

from fractions import gcd
def rational(n, d):
     """Construct a rational number x that represents n/d"""
     g = gcd(n, d)
     return (n//g, d//g)

from operator import getitem

def numer(x):
     """Return the numerator of rational number x"""
     return getitem(x, 0)

def denom(x):
     """Return the denominator of rational number x"""
     return getitem(x, 1)
