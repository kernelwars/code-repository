def getWordScore(word, n):
    """
    Returns the score for a word. Assumes the word is a valid word.

    The score for a word is the sum of the points for letters in the
    word, multiplied by the length of the word, PLUS 50 points if all n
    letters are used on the first turn.

    Letters are scored as in Scrabble; A is worth 1, B is worth 3, C is
    worth 3, D is worth 2, E is worth 1, and so on (see SCRABBLE_LETTER_VALUES)

    word: string (lowercase letters)
    n: integer (HAND_SIZE; i.e., hand size required for additional points)
    returns: int >= 0
    """
    # TO DO ... <-- Remove this comment when you code this function
    score = 0
    
    for mywords in word:
        score += SCRABBLE_LETTER_VALUES[mywords]
    if len(word) != n:
        score = score * len(word)
        return score
    else:
        score = score * len(word)
        score += 50
        return score

def updateHand(hand, word):
    """
    Assumes that 'hand' has all the letters in word.
    In other words, this assumes that however many times
    a letter appears in 'word', 'hand' has at least as
    many of that letter in it. 

    Updates the hand: uses up the letters in the given word
    and returns the new hand, without those letters in it.

    Has no side effects: does not modify hand.

    word: string
    hand: dictionary (string -> int)    
    returns: dictionary (string -> int)
    """
    # TO DO ... <-- Remove this comment when you code this function
    final = hand.copy()
    for letter in word:
        if letter in final:
            val = final[letter]
            final[letter] = val -1
            hand = final
    return hand
    
def isValidWord(word, hand, wordList):
    """
    Returns True if word is in the wordList and is entirely
    composed of letters in the hand. Otherwise, returns False.

    Does not mutate hand or wordList.
   
    word: string
    hand: dictionary (string -> int)
    wordList: list of lowercase strings
    """
    # TO DO ... <-- Remove this comment when you code this function
    for char in word:
        if char not in hand:
            return False
    if word in wordList:
        return True
    else:
        return False
def calculateHandlen(hand):
    """ 
    Returns the length (number of letters) in the current hand.
    
    hand: dictionary (string int)
    returns: integer
    """
    # TO DO... <-- Remove this comment when you code this function
    count = 0
    for element in hand:
        if hand[element] == 0:
            count += 0
        elif hand[element]>=1:
            count += hand[element]
    return count

def playGame(wordList):
    """
    Allow the user to play an arbitrary number of hands.
 
    1) Asks the user to input 'n' or 'r' or 'e'.
      * If the user inputs 'n', let the user play a new (random) hand.
      * If the user inputs 'r', let the user play the last hand again.
      * If the user inputs 'e', exit the game.
      * If the user inputs anything else, tell them their input was invalid.
 
    2) When done playing the hand, repeat from step 1
    """
    # TO DO ... <-- Remove this comment when you code this function
    global HAND_SIZE
    n = HAND_SIZE
    hand = []
    while True:
        choose = raw_input('Enter n to deal a new hand, r to replay the last hand, or e to end game: ')
        if choose == 'n':
            hand = dealHand(n)
            playHand(hand.copy(), wordList, n) # copy created but not kept locally  
            print ''
        elif choose == 'r':
            if len(hand)<1:  
                print 'You have not played a hand yet. Please play a new hand first!'
            else:
                playHand(hand.copy(), wordList, n)
        elif choose == 'e':
            break
        elif choose!='n' or choose!='r' or choose!='e':
            print 'Invalid command.'
            print ''

def compChooseWord(hand, wordList):
    """
    Given a hand and a wordList, find the word that gives 
    the maximum value score, and return it.

    This word should be calculated by considering all the words
    in the wordList.

    If no words in the wordList can be made from the hand, return None.

    hand: dictionary (string -> int)
    wordList: list (string)
    returns: string or None
    """
    # BEGIN PSEUDOCODE (available within ps4b.py)
    bestscore=0
    score=0
    bestword=None
    
    for words in wordList:
         if isValidWord(words, hand, wordList):
            score=getWordScore(words,sum(hand.values()))
            if score>bestscore:
                bestscore = score
                bestword = words
                
            
    return bestword

            
     
