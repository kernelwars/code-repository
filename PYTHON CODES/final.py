##import pylab
##
##a = 3.0
##b = 2.0
##c = 1.0
##yVals = []
##xVals = range(-20, 20)
##for x in xVals:
##    yVals.append(a*x**2 + b*x + c)
##yVals = pylab.array(yVals)
##xVals = 2*pylab.array(xVals)
##try:
##    a, b, c, d = pylab.polyfit(xVals, yVals, 3)
##    print a, b, c, d
##except:
##    print 'unable to fit'
##class Person(object):
##    def __init__(self, name):
##        self.name = name
##    def say(self, stuff):
##        return self.name + ' says: ' + stuff
##    def __str__(self):
##        return self.name
## 
##class Lecturer(Person):
##    def lecture(self, stuff):
##        return 'I believe that ' + Person.say(self, stuff)
## 
##class Professor(Lecturer):
##    def say(self, stuff):
##        return self.name + ' says: ' + self.lecture(stuff)
## 
##class Singer(Lecturer):
##    def sing(self, stuff):
##        return stuff + ' Tra la la'
##    def lecture(self, stuff):
##        return self.sing(Lecturer.lecture(self, stuff))
## 
##class ArrogantProfessor(Professor):
##    def say(self, stuff):
##        return 'It is obvious that ' + self.lecture(stuff)
## 
##e = Person('eric')
##le = Lecturer('eric')
##pe = Professor('eric')
##se = Singer('eric')
##ae = ArrogantProfessor('eric')
## 
## 
####print "1: " + e.say('the sky is blue')
####print "3: " + le.say('the sky is blue')
####print "4: " + le.lecture('the sky is blue')
####print "5: " + pe.say('the sky is blue')
####print "6: " + pe.lecture('the sky is blue')
####print "2: " + e.lecture('the sky is blue')
####print "7: " + pe.sing('the sky is blue')
##print "8: " + se.say('the sky is blue')
##print "9: " + se.lecture('the sky is blue')
##print "10: " + se.sing('the sky is blue')
##print "11: " + ae.say('the sky is blue')
##print "12: " + ae.lecture('the sky is blue')
import pylab
 
class Location(object):
    def __init__(self, x, y):
        """x and y are floats"""
        self.x = x
        self.y = y
       
    def move(self, deltaX, deltaY):
        """deltaX and deltaY are floats"""
        return Location(self.x + deltaX, self.y + deltaY)
   
    def getX(self):
        return self.x
   
    def getY(self):
        return self.y
   
    def distFrom(self, other):
        ox = other.x
        oy = other.y
        xDist = self.x - ox
        yDist = self.y - oy
        return (xDist**2 + yDist**2)**0.5
   
    def __str__(self):
        return '<' + str(self.x) + ', ' + str(self.y) + '>'
 
class Field(object):
    def __init__(self):
        self.drunks = {}
       
    def addDrunk(self, drunk, loc):
        if drunk in self.drunks:
            raise ValueError('Duplicate drunk')
        else:
            self.drunks[drunk] = loc
           
    def moveDrunk(self, drunk):
        if not drunk in self.drunks:
            raise ValueError('Drunk not in field')
        xDist, yDist = drunk.takeStep()
        currentLocation = self.drunks[drunk]
        #use move method of Location to get new location
        self.drunks[drunk] = currentLocation.move(xDist, yDist)
       
    def getLoc(self, drunk):
        if not drunk in self.drunks:
            raise ValueError('Drunk not in field')
        return self.drunks[drunk]
 
 
import random
 
class Drunk(object):
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return 'This drunk is named ' + self.name
class UsualDrunk(Drunk):
    def takeStep(self):
        stepChoices =\
            [(0.0,1.0), (0.0,-1.0), (1.0, 0.0), (-1.0, 0.0)]
        return random.choice(stepChoices)
 
class ColdDrunk(Drunk):
    def takeStep(self):
        stepChoices =\
            [(0.0,0.9), (0.0,-1.03), (1.03, 0.0), (-1.03, 0.0)]
        return random.choice(stepChoices)
 
class EDrunk(Drunk):
    def takeStep(self):
        ang = 2 * math.pi * random.random()
        length = 0.5 + 0.5 * random.random()
        return (length * math.sin(ang), length * math.cos(ang))
 
class PhotoDrunk(Drunk):
    def takeStep(self):
        stepChoices =\
                    [(0.0, 0.5),(0.0, -0.5),
                     (1.5, 0.0),(-1.5, 0.0)]
        return random.choice(stepChoices)
 
class DDrunk(Drunk):
    def takeStep(self):
        stepChoices =\
                    [(0.85, 0.85), (-0.85, -0.85),
                     (-0.56, 0.56), (0.56, -0.56)]
        return random.choice(stepChoices)
 
 
 
 
 
def walkVector(f, d, numSteps):
    start = f.getLoc(d)
    for s in range(numSteps):
        f.moveDrunk(d)
    return(f.getLoc(d).getX() - start.getX(),
           f.getLoc(d).getY() - start.getY())
 
def simWalks(numSteps, numTrials):
    homer = PhotoDrunk('Homer')
    origin = Location(0, 0)
    x=[]
    y=[]
    for t in range(numTrials):
        f = Field()
        f.addDrunk(homer, origin)
        a,b=walkVector(f, homer, numSteps)
        x.append(a)
        y.append(b)
    return x,y
 
import math
 
def drunkTest(numTrials = 1000):
    x,y=simWalks(1000,numTrials)
    pylab.plot(x,y,'ro')
    pylab.xlim(xmin=-100, xmax=100)
    pylab.ylim(ymin=-100, ymax=100)
    pylab.show()
