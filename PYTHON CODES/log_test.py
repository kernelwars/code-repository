def myLog(x, b):
    '''
    x: a positive integer
    b: a positive integer

    returns: log_b(x), or, the logarithm of x relative to a base b.
    '''
    # Your Code Here
    result = 0
    epsilon = .001
    if b == x:
        return 1
        
    while b ** epsilon < x:
        epsilon += .001
    return epsilon
        
myLog(15,3)
