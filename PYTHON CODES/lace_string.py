def laceStrings(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length, 
    then the extra elements should appear at the end.
    """
    # Your Code Here
    copy1 = s1
    copy2 = s2
    
    
    s3 = []
    if s1 == '':
        
        #PLACE A LINE OF CODE HERE
        return s2
    if s2 == '':
        #PLACE A LINE OF CODE HERE
        return s1
    
    while len(copy1)and len(copy2)!=0:
        
        s3.append(copy1[0])
        copy1 = copy1.replace(copy1[0],'')
        s3.append(copy2[0])
        copy2 = copy2.replace(copy2[0],'')
    if len(copy2) > len(copy1):
       s3 += copy2[0:]
    return ''.join(s3)

