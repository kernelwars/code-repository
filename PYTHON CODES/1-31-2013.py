def sumOfN(n):
    theSum = 0
    for i in range(1,n+1):
        theSum = theSum + i

    return theSum
print (sumOfN(10))

def sumofn(n):
    return (n*(n+1))/2

print (sumofn(10))


##Anagram problem. One string is an anagram of another if the second is simply
## a rearrangement of the first. For example 'heart' and 'earth'

def anagramSolution1(s1, s2):
    aList = list(s2)

    pos1 = 0
    stillOK = True

    while pos1 < len(s1) and stillOK:
        pos2 = 0
        found = False
        while pos2 < len(aList) and not found:
            if s1[pos1] == aList[pos2]:
                found = True
            else:
                pos2 = pos2 + 1
        if found:
            aList[pos2] = None
        else:
            stillOK = False

        pos1 = pos1 + 1


##Another approach to the anagram problem

def anagramSolution2(s1, s2):
    alist1 = list(s1)
    alist2 = list(s2)

    alist1.sort()
    alist2.sort()

    pos = 0
    matches = True
    while pos < len (s1) and matches:
        if alist1[pos] == alist2[pos]:
            pos = pos + 1
        else:
            matches = False

    return matches

print (anagramSolution2('heart', 'earth'))

##Count and compare approach to the anagram problem

def anagramSolution3(s1, s2):
    c1 = [0] * 26
    c2 = [0] * 26

    for i in range(len(s1)):
        pos = ord (s1[i]) - ord('a')
        c1[pos] = c1[pos] + 1

    for i in range (len(s2)):
        pos =  ord(s2[i]) - ord('a')
        c2[pos] = c2[pos] + 1

        j = 0
        stillOK = True

        while j < 26 and stillOK:
            if c1[j] == c2[j]:
                j = j + 1
            else:
                stillOK = False
        return stillOK

print anagramSolution3('heart','earth')

#IMplementing a stack

class Stack:

    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()
    def peek(self):
        return self.items[len(self.items)-1]

    def size (self):
        return len(self.items)

    
s=Stack()
print(s.isEmpty())
s.push(4)
s.push('dog')
print(s.peek())
s.push(True)
print(s.size())
print(s.isEmpty())
s.push(8.4)
print(s.pop())
print(s.pop())
print(s.size())    
