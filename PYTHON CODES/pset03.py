def evaluatePoly(poly, x):
    '''
    Computes the value of a polynomial function at given value x. Returns that
    value as a float.
 
    poly: list of numbers, length > 0
    x: number
    returns: float
    '''
    # FILL IN YOUR CODE HERE...
    result=0.0
    for i in range(len(poly)):
        result += poly[i]*x**i
    return float(result)

def computeDeriv(poly):
    '''
    Computes and returns the derivative of a polynomial function as a list of
    floats. If the derivative is 0, returns [0.0].
 
    poly: list of numbers, length &gt; 0
    returns: list of numbers (floats)
    '''
    # FILL IN YOUR CODE HERE...
    
    power = 0
    derv = []
    for val in poly:
        if len(poly)==1:
            derv.insert(power,float(val * power))
            return derv
        else:
            derv.insert(power, float(val * power))
            power +=1
    return derv[1:]


def computeRoot(poly, x_0, epsilon):
    ans = 0
    ansDeriv = 0
    power = 1
    numIterations = 0
    currentx = x_0
    while True:
        ansDeriv = 0
        ans = evaluatePoly(poly, currentx)
        derivList = computeDeriv(poly)
        for derivItem in derivList:
            ansDeriv = evaluatePoly(derivList, currentx)
            power += 1
        if abs(ans) >= epsilon:
            currentx = currentx - (ans/ansDeriv)
            power = 1
        else:
            break
        numIterations += 1
    return [currentx, numIterations]

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    for i in secretWord:
        if i not in lettersGuessed:
            return False
    return True

def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...
    print 'Welcome to the game Hangman!'
    length=len(secretWord)
    print 'I am thinking of a word that is '+str(length)+' letters long'
    i=8
    lettersGuessed=''
    

    while i>0 :
        print'-----------'
        print 'You have '+str(i)+' guesses left'
        print 'Available Letters: '+getAvailableLetters(lettersGuessed)
        guess=raw_input('Please guess a letter: ')
        guess1=guess.lower()
        lettersGuessed2=lettersGuessed+guess1
        

        if guess1 in secretWord:
            print 'Good guess: '+getGuessedWord(secretWord, lettersGuessed2)
        elif guess1 in lettersGuessed:
            print 'Oops! You\'ve already guessed that letter: '+getGuessedWord(secretWord, lettersGuessed2)
        else:
            print 'Oops! That letter is not in my word: '+getGuessedWord(secretWord, lettersGuessed2)
            i-=1

        lettersGuessed=lettersGuessed2

        if isWordGuessed(secretWord, lettersGuessed)==True:
            print'-----------'
            print 'Congratulations, you won!'
            break
    
    if i==0:
        print'-----------' 
        print 'Sorry, you ran out of guesses. The word was '+str(secretWord)+'.'
# When your hangman function passes the checks in the previous
# box, paste your function definition here to test it on harder 
# input cases.
def hangman(secretWord):
    print 'Welcome to the game Hangman!'
    length=len(secretWord)
    print 'I am thinking of a word that is '+str(length)+' letters long'
    i=8
    lettersGuessed=''
    

    while i>0 :
        print'-----------'
        print 'You have '+str(i)+' guesses left'
        print 'Available Letters: '+getAvailableLetters(lettersGuessed)
        guess=raw_input('Please guess a letter: ')
        guess1=guess.lower()
        lettersGuessed2=lettersGuessed+guess1
        

        if guess1 in secretWord:
            print 'Good guess: '+getGuessedWord(secretWord, lettersGuessed2)
        elif guess1 in lettersGuessed:
            print 'Oops! You\'ve already guessed that letter: '+getGuessedWord(secretWord, lettersGuessed2)
        else:
            print 'Oops! That letter is not in my word: '+getGuessedWord(secretWord, lettersGuessed2)
            i-=1

        lettersGuessed=lettersGuessed2

        if isWordGuessed(secretWord, lettersGuessed)==True:
            print'-----------'
            print 'Congratulations, you won!'
            break
    
    if i==0:
        print'-----------' 
        print 'Sorry, you ran out of guesses. The word was '+str(secretWord)+'.'


                   
