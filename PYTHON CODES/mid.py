def laceStrings(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length, 
    then the extra elements should appear at the end.
    """
    # Your Code Here
    copy1 = s1
    copy2 = s2
    
    
    s3 = []
    if s1 == '':
        
        
        #PLACE A LINE OF CODE HERE
        return s2
    if s2 == '':
        #PLACE A LINE OF CODE HERE
        return s1
    
    while len(copy1)and len(copy2)!=0:
        
        s3.append(copy1[0])
        copy1 = copy1.replace(copy1[0],'')
        s3.append(copy2[0])
        copy2 = copy2.replace(copy2[0],'')
    if len(copy2) > len(copy1):
        s3 += copy2[0:]
        
    return ''.join(s3)

def laceStringsRecur(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length, 
    then the extra elements should appear at the end.
    """
    def helpLaceStrings(s1, s2, out):
        if s1 == '':
            #PLACE A LINE OF CODE HERE
            return out + s2
        if s2 == '':
            #PLACE A LINE OF CODE HERE
            return out + s1
        else:
            #PLACE A LINE OF CODE HERE
            return helpLaceStrings(s2, s1[1:], out + s1[0])
            
    return helpLaceStrings(s1, s2, '')
def fixedPoint(f, epsilon):
    """
    f: a function of one argument that returns a float
    epsilon: a small float
  
    returns the best guess when that guess is less than epsilon 
    away from f(guess) or after 100 trials, whichever comes first.
    """
    guess = 1.0
    for i in range(100):
        if f(guess) - guess > epsilon:
            return (guess + i/guess) / 2
        else:
            guess = f(guess)
    return abs(guess)
def sqrt(a):
    def tryit(x):
        return 0.5 * (a/x + x)
    return fixedPoint(sqrt, 0.0001)

def babylon(a):
    def test(x):
        return 0.5 * ((a / x) + x)
    return test

def sqrt(a):
    return fixedPoint(babylon, 0.0001)
