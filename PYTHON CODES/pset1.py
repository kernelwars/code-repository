balance = float(raw_input("What  is the balance "))
annualInterestRate = float(raw_input("What is the anual interest rate? "))
monthlyPaymentRate = float(raw_input("What is the payment rate? "))
totalpaid = 0

for month in range(1,13):
     mpr = balance * monthlyPaymentRate
     mir = (balance - mpr) * annualInterestRate/12
     totalpaid = totalpaid + mpr
     balance = balance - mpr + mir
     print('Month: ' + str(month))
     print('Minimum monthly payment: ' + str(round(mpr,2)))
     print('Remaining balance: ' + str(round(balance,2)))
print('Total paid: ' + str(round(totalpaid,2)))
print('Remaining balance: ' + str(round(balance,2)))
