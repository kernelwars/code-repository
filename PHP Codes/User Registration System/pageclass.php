<?php
/**
 *
 */
require_once('baseclasstesting.php');
class CreateAccountPage extends pagebase{

    public function title(){
        echo "MOO";
    }

    protected function generateBodyContents(){

        echo <<<EOCONTENTS
    <h3>Create a New user Account</h3>
    <form action="submitaccountdatapage.php" method="post" name="create_user_form">
    <div>
        <label>User Name:</label>
        <input type="text" name="user_name" size="30">
    </div>
    <div>
        <label>Full Name:</label>
        <input type="text" name="full_name" size="60">
    </div>
    <div>
        <label>Password:</label>
        <input type="password" name="password1" size="20">
    </div>
    <div>
        <label>Confirm Password:</label>
        <input type="password" name="password2" size="20">
    </div>
    <div>
        <label>Email Address:</label>
        <input type="text" name="email_address" size="20">
    </div>


    <p><input type="submit" value="Create User"></p>

</form>



EOCONTENTS;


    }
    public function inlineStyle(){

        return <<<EOSTYLE
            label{
            margin-top: 0.5em;
            display: block;
            font-family: Arial,Helvetica;
            font-size : 10pt;
            color: #444;
        }

EOSTYLE;


    }




}

$page = new CreateAccountPage();
$page->processRequest();

class SubmitAccountDataPage extends pagebase{

    protected $m_error;
    protected $m_createdUser;

    public function title(){
        echo "Submitted";
    }

    protected function generateBodyContents(){

        if($this->m_error != null){
            echo <<<EOM

             <p class='error_msg'>$err</p>

EOM;

        }else{
            echo "<h3>Account create successfully</h3>\n";
            echo "<div id ='details_header'>Here are the account details</div> ";
            echo "<pre>\n";
            $this->m_createdUser->debugPrint();
            echo "</pre>\n";
        }

    }

    protected function processIncomingFormData(){

        if (!isset($_POST['user_name']) or trim($_POST['user_name'])=='')
            $this->m_error = "You mujst specify a user name";
        else if(!isset($_POST['full_name']) or trim($_POST['full_name'])=='')
            $this->m_error = "You must specify a full name for your account";
        elseif(!isset($_POST['email_address']) or trim($_POST['email_address'])=='')
            $this->m_error = "Please provide an email address";
        elseif(!isset($_POST['password1']) or trim($_POST['password1'])!=trim($_POST['password2']))
            $this->m_error = "The passwords provided are either invalid or do not match";
        else
            $this->m_error = null;

        if ($this->m_error == null){
            $un = $_POST['user_name'];
            $fn = $_POST['full_name'];
            $pw1 = $_POST['password1'];
            $em = $_POST['email_address'];

            $this->m_createdUser = new User($un,$fn,$pw1,$em);

        }



    }
    public function inlineStyle(){

        return <<<EOSTYLE
            label{
            margin-top: 0.5em;
            display: block;
            font-family: Arial,Helvetica;
            font-size : 10pt;
            color: #444;
        }

EOSTYLE;


    }
}
$page = new SubmitAccountDataPage();
$page->processRequest();


?>