<?php
/****************************************************/
/* CoffeeCup Software News Flash                    */
/* (C) 2005 CoffeeCup Software                      */
/****************************************************/
/* - Companion Program For News Flash - */
/* Visit - http://www.coffeecup.com                 */
/****************************************************/
/* Constants   */
/* Version     */ $version = '1.1';
//only works with php 4.0.3 and above

$dataURL = urldecode($_REQUEST['url']);
readfile($dataURL);

//XML file must be in the same directory as this file
$debug = (isset($_REQUEST['debug'])) ? $_REQUEST['debug'] : $debug;
if ($debug) error_reporting(E_ALL);

//...... Display debugging information
if ($debug)
{
        switch($debug)
        {
                case 'info'    :
                   phpinfo();
                   exit();
                break;

        }
}

?>