USE AdventureWorks2012;
GO
--No input parameter sp_help
EXEC [dbo].[sp_help];

--Evaluate [Person].[Address] table
EXEC [dbo].[sp_help] N'[Person].[Address]';

--Row Insert
--INSER [INTO] TABLE_OR_VIEW [(COLUMN_lIST)] DATA_VALUES;
INSERT INTO Production.ProductCategory ([Name])
VALUES ('Software');
GO
--Find out the other columns
Select [ProductCategoryID], [Name],[rowguid],[ModifiedDate]
from Production.ProductCategory
where [Name] = 'Software'
--Find out the properties of the table
EXEC sys.sp_help 'Production.Productcategory';
GO
--Creating a new table
Create Table [Production].[ProductCategoryV2]
	(
		[ProductCategoryID] [int] Not Null,
		[Name] [dbo].[Name] Not Null,
		[rowguid] [uniqueidentifier] Not Null,
		[ModifiedDate] [datetime] Not Null
	
	)
ON [PRIMARY];
GO
--Inserting Data
Insert Into [Production].ProductCategoryV2
VALUES (1, 'Appliances', NEWID(), GETDATE());
GO
Select * from Production.ProductCategoryV2
--Modifying the original table information
ALter Table [Production].[ProductCategoryV2]
ADD [Enabled] BIT NULL;
GO
--Inserting
INSERT INTO [Production].ProductCategoryV2
VALUES (2, 'E-Books', NewID(), GetDate(), 1)

--Insert three rows in one statement
INSERT INTO [Production].Culture
(CultureID,Name,ModifiedDate)
Values ('af', 'Afrikaans', DEFAULT),
		('hy','Armenian', DEFAULT),
		('sd', 'sindhi', DEFAULT);
		
SELECT CultureID, Name, ModifiedDate
from Production.Culture
order by ModifiedDate;
GO	

--Creating a resultset on the fly
SELECT [CULTUREID], [NAME] 
FROM 
	(VALUES ('tg', 'Tajik'),('hy','Armenian'))
	AS [Culture] ([CultureID], [Name]);	



