USE ShadowTangent;
SELECT prod_name from dbo.products;

SELECT prod_name, prod_name, prod_price from products

--Retrieving All Columns
Select * from products

Select vend_id from products

--Return only distinct values

SELECT DISTINCT vend_id from products
--Limiting results

SELECT TOP(5) prod_name from products
--Retrieving percentage of rows

SELECT TOP (25) percent prod_name from products

--Fully Quantified table names

SELECT products.prod_name from products

--SORTING 
SELECT prod_name from products ORDER BY prod_name

--SORT BY MULTIPLE COLUMNS

SELECT prod_id, prod_name, prod_price from products
ORDER BY prod_name, prod_price